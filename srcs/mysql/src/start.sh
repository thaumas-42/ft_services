mysql_install_db --user=root --ldata=/var/lib/mysql
cat > /var/lib/mysql.tmp << eof
CREATE DATABASE IF NOT EXISTS wordpress;
CREATE USER IF NOT EXISTS 'phpmyadmin'@'%' IDENTIFIED BY 'phpmyadmin';
CREATE USER IF NOT EXISTS 'wordpress'@'%' IDENTIFIED BY 'wordpress';
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'%' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
QUIT;
eof

telegraf &
/usr/bin/mysqld --init_file=/var/lib/mysql.tmp --user=root
#/usr/bin/mysqld --console --init_file=/tmp/sql --user=root
#GRANT CREATE, ALTER, DROP, INSERT, UPDATE, DELETE, SELECT ON *.* TO 'wordpress'@'%' WITH GRANT OPTION;
#GRANT CREATE, ALTER, DROP, INSERT, UPDATE, DELETE, SELECT ON *.* TO 'phpmyadmin'@'%' WITH GRANT OPTION;
