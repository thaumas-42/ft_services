#!/usr/bin/env bash
clear
minikube delete

OS="`uname`"

case $OS in
	"Linux")
		minikube start --driver=docker
		sed -i "s/banana-split/172.17.0.10-172.17.0.19/g" srcs/k8s/configmap-metallb.yml
		sed -i "s/42-life/172.17.0.20-172.17.0.20/g" srcs/k8s/configmap-metallb.yml
		sed -i "s/192.168.99.111:5050/172.17.0.11:5050/g" srcs/mysql/src/wordpress.sql
		sed -i "s/patate-carotte/172.17.0.21-172.17.0.21/g" srcs/k8s/configmap-metallb.yml
		FTPSIP=172.17.0.21
	;;
	"Darwin")
		minikube start --driver=virtualbox
		sed -i '' "s/banana-split/192.168.99.110-192.168.99.119/g" srcs/k8s/configmap-metallb.yml
		sed -i '' "s/42-life/192.168.99.120-192.168.99.120/g" srcs/k8s/configmap-metallb.yml
		sed -i '' "s/172.17.0.11:5050/192.168.99.111:5050/g" srcs/mysql/src/wordpress.sql
		sed -i '' "s/patate-carotte/192.168.99.121-192.168.99.121/g" srcs/k8s/configmap-metallb.yml
		FTPSIP=192.168.99.121
	;;
	*) ;;
esac

eval $(minikube docker-env)

minikube addons enable metallb

kubectl replace -f srcs/k8s/configmap-metallb.yml

docker build -t mysql-img srcs/mysql
kubectl apply -f srcs/k8s/mysql.yaml
docker build -t influxdb-img srcs/influxdb
kubectl apply -f srcs/k8s/influxdb.yaml
docker build -t nginx-img srcs/nginx
kubectl apply -f srcs/k8s/nginx.yaml
docker build -t grafana-img srcs/grafana
kubectl apply -f srcs/k8s/grafana.yaml
docker build -t ftps-img --build-arg IP=${FTPSIP} srcs/ftps
kubectl apply -f srcs/k8s/ftps.yaml
docker build -t phpmyadmin-img srcs/phpmyadmin
docker build -t wordpress-img srcs/wordpress

kubectl exec -i $(kubectl get pods | grep mysql | cut -d" " -f1) -- mysql wordpress -u root < srcs/mysql/src/wordpress.sql
kubectl apply -f srcs/k8s/wordpress.yaml
kubectl apply -f srcs/k8s/phpmyadmin.yaml

echo \
"  |service | user     | pass     |
  |--------|----------|----------|
  |PMA     |phpmyadmin|phpmyadmin|
  |--------|----------|----------|
  |WP      |wordpress |wordpress |
  |--------|----------|----------|
  |ftps    |user      |pass      |
  |--------|----------|----------|
  |grafana |admin     |admin     |
  |--------|----------|----------|
  |ssh     |admin     |admin     |
  |--------|----------|----------|
"
minikube dashboard &

