<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'mysql-service' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zWc,bD|9OKLyMO7EY(BoW/}G}S7:k_g{vl-=5_PXkZ*Gh::Pqi~(LdCOo?+>Z*Ga');
define('SECURE_AUTH_KEY',  'SH[T8e<th J19pp3Pm_(1nPojc4HuvO%YzYkt7y]ov?q021LhWSqTRNy}=2tHZ4Y');
define('LOGGED_IN_KEY',    '(}HGU1*Ocnh:,`m4^7NF{&o=Y(Q<4C-L+W0D9_wBwUMIUR-56axz+B EU{4H>1N9');
define('NONCE_KEY',        'aA/!m7S;v<*`K6KNV.[j#~p;UzKm;DIT$x)ib+]hZ=ZlO<T}*26?`4bls)Wt+^L=');
define('AUTH_SALT',        'ygiM^90EPBh XPn+-Q|hRa_$B6?dG*9+;~pz=!;Z-lspgVY)|+i?i:sy~AKYP~]O');
define('SECURE_AUTH_SALT', '->ze%v.PlnGB/8H*Q*$@34|b4Nw~S;v]Ntk~Q ]tJ}Awg,Yz<OL65K}85P6O)mW+');
define('LOGGED_IN_SALT',   'OXSD%-nN9mre/Pn?] Yo Y]GzEtZ<Hepe.!&fz3QQ_,13Ly(7::=mT$2@{Vo%J Y');
define('NONCE_SALT',       'vOLkdLqDb~_$1^+hU,b<Th[Zm3pCNe=0Cp{B&=%okoUyB}8rg>;C(lt-/7CB=D 2');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
